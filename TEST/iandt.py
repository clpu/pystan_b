﻿# -*- coding: utf-8 -*-
"""
This is the test module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload

import os

import numpy as np

# EXTEND PATH
# ..

# INTERNAL
import image as image      # !! functional routines applicable to many cases

import plotter as plotter

import physics as physics  # !! physics routines applicable to many cases

# RELOAD
reload(image)
reload(plotter)
reload(physics)

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================
# canvas
global_shape = (600,800)
global_dtype = np.uint8

global_pixel_to_m = 1.e-3

global_noise = 50
global_canvas = 70

# test magnet

# test spectrum

# ==============================================================================
# FUNCTIONS
# ==============================================================================
def prepare_test(path):
    global global_shape, global_dtype
    print('\nPREPARE INTEGRATION TEST')
    # reference image
    reference = prepare_test_ref(global_shape,global_dtype)
    zero_pointer = image.GIVE_MASK("ELLIPTICAL",global_shape,\
                             corner_x = int(global_shape[1]/3.5), corner_y = int(global_shape[0]/2),\
                             width = 10,    height = 10)
    reference_bis = reference + zero_pointer * 100
    # save references
    ref_data_path = path+os.path.sep+'TEST'+os.path.sep+"ref.tif"
    image.IMWRITE(ref_data_path,reference)
    ref_data_path = path+os.path.sep+'TEST'+os.path.sep+"ref_zero.tif"
    image.IMWRITE(ref_data_path,reference_bis)
    # add syntetic data
    # .. !TODO physics solver for spectrum through magnet, for now:
    trace = image.GIVE_MASK("RECTANGULAR",global_shape,\
                             corner_x = int(global_shape[1]/3), corner_y = int(global_shape[0]/2),\
                             width = int(global_shape[1]/3),    height = 10)
    testdata = reference + trace * 100
    # save test data
    test_data_path = path+os.path.sep+'TEST'+os.path.sep+'DATA'+os.path.sep+"trace.tiff"
    image.IMWRITE(test_data_path,testdata.astype(global_dtype))
    # return test bed
    return {'test_data':testdata, 'ref':reference, 'zero':reference_bis}
    
def prepare_test_ref(shape,dtype):
    global global_noise,global_canvas
    # create noise
    noise = (np.random.random_sample(shape[0]*shape[1]) * global_noise).reshape(shape)
    # create spectrometer
    screen = image.GIVE_MASK("RECTANGULAR",shape,\
                             corner_x = int(shape[1]/4), corner_y = int(shape[0]/4),\
                             width = int(shape[1]/2),    height = int(shape[0]/2))
    mask = image.GIVE_MASK("RECTANGULAR",shape, dia = "-",\
                             corner_x = int(shape[1]/4), corner_y = int(shape[0]/4),\
                             width = int(shape[1]/2),    height = int(shape[0]/2))
    img = noise * mask + screen * global_canvas
    # output
    return img.astype(dtype)