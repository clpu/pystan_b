# -*- coding: utf-8 -*-
"""
USER INFORMATION TO BE FOUND IN THE FILE README.MD

project             pythonic spectral trace analysis: magnetic spectrometer
acronym             pystan_b
created on          2021-09-01-14:22:00

@author             Micha (MEmx), CLPU, Villamayor, Spain
@moderator          Alessandro Curcio (AC), CLPU, Villamayor, Spain
@updator            Micha (MEmx), CLPU, Villamayor, Spain
            
@contact            michael@michaelmaxx.de

interpreter         python > 3.8
version control     git

requires explicitely {
  - opencv
  - tifffile
  - matplotlib
  - scipy
  - scikit-image
  - imutils
}
  
which includes implicitely {
  - numpy
  - inspect
  - importlib
  - os
  - sys
  - time
}

input file {
  - CSV COLUMNS :: KEY, VALUE, HELP
  - CSV ROWS    :: ASSIGNMENTS {KEY => VALUE}
}

flags {
  - 
}

execute command: python run.py input_file.csv *args(flags :: {None,"-".string})

"""
# ==============================================================================
# IMPORT PYTHOqN MODULES
# ==============================================================================
# EXTERNAL
from importlib import reload        # !! management of self-made modules
from inspect import getsourcefile   # !! inquiry of script location

import os                           # !! methods of operating system
import sys                          # !! executions by operating system
import time                         # !! time and date

import numpy as np                  # !! relatively fast array operations
import cv2                          # !! operations on images
import imutils                      # !! math on images

import readchar                     # !! allows to read key input of stdin

# EXTEND PATH
root = os.path.dirname(os.path.abspath(getsourcefile(lambda:0))) # !! get environment
sys.path.append(os.path.abspath(root+os.path.sep+'LIB'))         # !! add library directory
sys.path.append(os.path.abspath(root+os.path.sep+'SCR'))         # !! add script directory
sys.path.append(os.path.abspath(root+os.path.sep+'TEST'))        # !! add test directory

# INTERNAL
from constants import *          # !! global constants from LIB/constants.py

import formats as formats        # !! global formats from LIB/formats.py

import iandt as integration_test # !! self test from TEST/iandt.py

import functions as func         # !! general collection of functions from SCR/functions.py
import loader as load            # !! routines fetching input data from SCR/loader.py
import plotter as plotter        # !! plotter routines from SCR/plotter.py
import exporter as export        # !! routines for saving output data from SCR/exporter.py
import image as image            # !! treatment of graphical data from SCR/image.py

import physics as physics        # !! physics calculations from SCR/physics.py

# ==============================================================================
# SET UP RUNTIME ENVIRONMENT
# ==============================================================================
# RELOAD INTERNAL MODULES
reload(export)
reload(formats)
reload(func)
reload(image)
reload(integration_test)
reload(load)
reload(physics)
reload(plotter)

# SEARCH FOR COMMAND LINE INPUT
args = func.COMMAND_LINE(['absolute_path_to_inputfile', \
                          'flags'])

# TAKE TIME
debut = time.time()

# PROMPT GREETINGS
print('\nWELCOME TO pySTAN_B')

# CLEARIFY RUN
script_path,work_path = func.ENVIRONMENT(root = root)

# ROOM FOR TESTS
# ..

# ==============================================================================
# LOAD DIAGNOSTIC'S SETUP
# ==============================================================================
# PARSE INPUT FILES
input_dict = load.parse_inputfile(args[1],[script_path,work_path])

# CLEARIFY OUTPUT DIRECTORY
if 'output_directory' not in input_dict.keys():
    input_dict['output_directory'] = root+os.path.sep+'usr/out'

# ==============================================================================
# BUILD OPTIONAL ENVIRONMENTS
# ==============================================================================
# TEST DATA
if 'test' in input_dict.keys():
    input_dict['test'] = integration_test.prepare_test(root)
    
# STORAGE SPACE
if 'output_directory' in input_dict.keys():
    export.check_isdir(input_dict['output_directory'])
    
# TEMPORARY FILES
if 'temp_dir' not in input_dict.keys():
    input_dict['temp_dir'] = root+os.path.sep+"TMP"
    export.check_isdir(input_dict['temp_dir'])
    
# ==============================================================================
# LOAD INPUT
# ==============================================================================
# PARSE DATA FILES
if load.USE_INPUT_KEY('run.py','data',input_dict):
    if load.USE_INPUT_KEY('run.py','online',input_dict):
        if input_dict["online"] == 'T':
            old_data = load.parse_datafiles(input_dict['data'],[script_path,work_path])
            input_data = []
        else:
            old_data = []
            input_data = load.parse_datafiles(input_dict['data'],[script_path,work_path])
    else:
        input_dict["online"] = 'F'
        old_data = []
        input_data = load.parse_datafiles(input_dict['data'],[script_path,work_path])
    
# PARSE REFERENCE FILES
# closest possible conditions as for experimental results - but without particles hitting screen
if load.USE_INPUT_KEY('run.py','reference',input_dict):
    if input_dict['reference'] == "None":
        reference_data = ""
    else:
        reference_data = load.parse_reference(input_dict['reference'],[script_path,work_path])
else:
    reference_data = np.nan
    
# electronic noise of camera
# .. !TODO

# SPATIAL CALIBRATION   
# absolute zero
if load.USE_INPUT_KEY('run.py','zero_calibration',input_dict) and not load.USE_INPUT_KEY('run.py','zero_point',input_dict):
    zero_calibration = load.parse_reference(input_dict['zero_calibration'],[script_path,work_path])
elif load.USE_INPUT_KEY('run.py','zero_point',input_dict):
    if input_dict['zero_point'] != "None":
        zero_calibration = ""
    elif load.USE_INPUT_KEY('run.py','zero_calibration',input_dict):
        zero_calibration = load.parse_reference(input_dict['zero_calibration'],[script_path,work_path])

# lineout
if load.USE_INPUT_KEY('run.py','lineout',input_dict):
    if input_dict['lineout'] != "None":
        manual_lineout = input_dict['lineout']
    else:
        manual_lineout = None
else:
    manual_lineout = None

# ==============================================================================
# CALIBRATION
# ==============================================================================
# ..

# ==============================================================================
# ANALYZE DATA
# ==============================================================================
# EXTRACT REFERENCE INFORMATION
# load reference image
if reference_data == "":
    ref = {"channels" : 0, "image": 0.}
elif image.ISIMG(reference_data): 
    ref = load.dataset(reference_data)
    plotter.imshow(ref['image'],name="Reference Data")
else:
    func.MESSAGE_PRINT('run.py',"No valid reference file defined!")
    func.ERROR_PRINT(2)

# reduce complexity of reference image
if ref['channels'] > 1:
    if load.USE_INPUT_KEY('run.py','channel',input_dict):
        ref['image'] = image.REDUCEDIMENSIONS(ref['image'],input_dict['channel'])
        ref['channels'] = 1

# load reference for absolute zero deflection
if zero_calibration == "":
    zero_str = input_dict['zero_point'].replace("[","").replace("]","").split(";")
    zero = {'channels':0,'data':[int(zero_str[0]),int(zero_str[1])]}
elif image.ISIMG(zero_calibration):
    zero = load.dataset(zero_calibration)
    plotter.imshow(zero['image'],name="Reference for Zero")
else:
    func.MESSAGE_PRINT('run.py',"No valid reference for the zero-point defined!")
    func.ERROR_PRINT(2)

# check for manual input of lineout
if manual_lineout != None:
    lineout_str_p1 = manual_lineout.split(":")[0]
    lineout_str_p1 = lineout_str_p1.replace("[","").replace("]","").split(";")
    lineout_str_p2 = manual_lineout.split(":")[1]
    lineout_str_p2 = lineout_str_p2.replace("[","").replace("]","").split(";")
    manual_lineout = np.array([lineout_str_p1,lineout_str_p2]).astype("int")
    del lineout_str_p1,lineout_str_p2
    
# reduce complexity of reference for absolute zero
if zero['channels'] > 1:
    if load.USE_INPUT_KEY('run.py','channel',input_dict):
        zero['image'] = image.REDUCEDIMENSIONS(zero['image'],input_dict['channel'])
        zero['channels'] = 1

# display options for lines and points, important for following dialogues
if load.USE_INPUT_KEY('run.py','linewidth',input_dict):
    linewidth = int(input_dict['linewidth'])
else:
    # .. !TODO user dialogue to get linewidth
    linewidth = 1
    pass

# extract absolute zero point
if zero['channels'] > 0:
    zero_xy = image.INTERACTIVE_POINT(zero['image'],adjust='T',radius=linewidth)
else:
    zero_xy = (zero['data'][0],zero['data'][1])
if len(zero_xy) > 2:
    # dirty fix for problem with some images !TODO
    zero_xy = (zero_xy[0],zero_xy[1])

# FORMALIZE ANALYSIS PROCEDURE
def analyze(input_file,input_dict, *args, **kwargs):
#    nonlocal manual_lineout
    global zero_xy, ref, linewidth
    fixed_lineout = kwargs.get('fixed_lineout', None)
    if not isinstance(manual_lineout,np.ndarray):
        if manual_lineout != None:
            fixed_lineout = manual_lineout
    else:
        fixed_lineout = manual_lineout
    # DIALOGUE
    # raise awareness
    print("\nANALYZE '"+input_file+"'")
    
    # LOAD INFORMATION
    # load image
    if image.ISIMG(input_file): 
        img = load.dataset(input_file)
    else:
        return False
    # subtract background ! .. TODO
    # ...
    # reduce complexity
    if img['channels'] > 1:
        if load.USE_INPUT_KEY('run.py','channel',input_dict):
            img['image'] = image.REDUCEDIMENSIONS(img['image'],input_dict['channel'])
            img['channels'] = 1
    # stock RAW
    image.IMWRITE(input_dict['temp_dir']+os.path.sep+"RAW.png",image.ENHANCED_VISIBILITY(img['image']))
    # stock RAW + zero
    # print(image.ENHANCED_VISIBILITY(img['image']).shape) # controll print
    image.IMWRITE(input_dict['temp_dir']+os.path.sep+"RAW_plus_zero.png",cv2.circle(image.ENHANCED_VISIBILITY(img['image']), zero_xy, linewidth, (255, 255, 255), -1))
    # stock RAW - reference
    image.IMWRITE(input_dict['temp_dir']+os.path.sep+"RAW_minus_ref.png",image.ENHANCED_VISIBILITY(img['image']-ref['image']))
    # locate spectrometer trace
    if fixed_lineout is None:
        # mark spectrometer trace
        axis_xy = image.INTERACTIVE_LINE(img['image'],adjust='T',linewidth=linewidth)
        img['lineout'] = axis_xy
    else:
        # loadposition of lastly analyzed trace
        axis_xy = fixed_lineout
    # line equation
    delta_xy = axis_xy[1] - axis_xy[0]
    # stock RAW + line
    image.IMWRITE(input_dict['temp_dir']+os.path.sep+"RAW_plus_line.png",cv2.line(image.ENHANCED_VISIBILITY(img['image']), (axis_xy[0][0],axis_xy[0][1]), (axis_xy[1][0],axis_xy[1][1]), (255, 255, 255), linewidth))
    
    # TREAT INFORMATION
    # rotate image
    angle = - np.arctan(delta_xy[1]/delta_xy[0])
    rotated = imutils.rotate_bound(img['image'], angle * 360./(2.*np.pi) )
    # calculate zero coordinates and axis coordinates in rotated frame
    rotaxis_xy = []
    rotzero_xy = []
    
    x_frame = 0.
    if angle > 0. : x_frame = img['image'].shape[0] * np.sin(angle)
    y_frame = 0.
    if angle < 0. : y_frame = - img['image'].shape[1] * np.sin(angle)
    
    x_x = axis_xy[0][0] * np.cos(angle)
    x_y = - axis_xy[0][1] * np.sin(angle)
    y_x = axis_xy[0][0] * np.sin(angle)
    y_y = axis_xy[0][1] * np.cos(angle)
    x_c = int(x_frame + x_x + x_y)
    y_c = int(y_frame + y_x + y_y)
    rotaxis_xy.append((x_c,y_c))
    
    x_x = axis_xy[1][0] * np.cos(angle)
    x_y = - axis_xy[1][1] * np.sin(angle)
    y_x = axis_xy[1][0] * np.sin(angle)
    y_y = axis_xy[1][1] * np.cos(angle)
    x_c = int(x_frame + x_x + x_y)
    y_c = int(y_frame + y_x + y_y)
    rotaxis_xy.append((x_c,y_c))
    
    rotaxis_xy = np.array(rotaxis_xy)
    
    x_x = zero_xy[0] * np.cos(angle)
    x_y = - zero_xy[1] * np.sin(angle)
    y_x = zero_xy[0] * np.sin(angle)
    y_y = zero_xy[1] * np.cos(angle)
    x_c = int(x_frame + x_x + x_y)
    y_c = int(y_frame + y_x + y_y)
    rotzero_xy = (x_c,y_c)
    
    # EXTRACT INFORMATION
    # extract lineout
    transverse = int(linewidth/2.)
    lower_i = rotaxis_xy[0][1] - transverse
    upper_i = rotaxis_xy[0][1] + transverse
    lower_j = min([rotzero_xy[0],rotaxis_xy[0][0],rotaxis_xy[1][0]])
    upper_j = max([rotzero_xy[0],rotaxis_xy[0][0],rotaxis_xy[1][0]])
    
    lineout = np.sum(rotated[lower_i:upper_i,lower_j:upper_j], axis = 0)/(2.*transverse)
    pixel_axis = np.arange(lower_j,upper_j) - rotzero_xy[0]
    
    # plot lineout
    print("> PLOT LINEOUT")
    plotter.plot2D(pixel_axis,lineout,input_dict['temp_dir']+os.path.sep+"lineout.png",x_label = "Position / [pix]", y_label = "Grayvalue", silent = True)
    if 'output_directory' in input_dict.keys():
        plotter.plot2D(pixel_axis,lineout,input_dict['output_directory']+os.path.sep+func.STRIP_EXTENSION(os.path.basename(input_file))+"_lineout.png", silent = True)
    else:
        print("> PLOT LINEOUT")
        plotter.plot2D(pixel_axis,lineout,None)
    
    # give expected uncertainty from misspointing compared to zero reference
    # .. !TODO
    
    # DERIVE SPECTRUM
    img['physics'] = physics.give_energyspectrum(input_dict,pixel_axis,lineout,input_file)
        
    # !.. !TODO
    # return spectrum
    return img

# FORMALIZE VISUALIZATION AND SAVING PROCEDURE
def write_out(img,*args,**kwargs):
    silent = kwargs.get('full_analysis',False)
    # visualize results
    if 'physics' in img.keys() and not silent:
        if load.USE_INPUT_KEY('run.py','spectrum',img['physics']):
            # .. !TODO
            pass
        else:
            func.MESSAGE_PRINT('run.py',"DO NOT FIND RESULTS FOR PLOTTER!")
    else:
        func.MESSAGE_PRINT('run.py',"DO NOT FIND RESULTS FOR",img['source'])
    # save results
    np.savetxt(input_dict['output_directory']+os.path.sep+func.STRIP_EXTENSION(os.path.basename(img['source']))+"_spectrum.dat",
               np.array([img["physics"]["energy_axis"].astype(float)/MeV_to_J,img["physics"]["spectrum"].astype(float)*MeV_to_J]).T,
               header="energy_in_MeV spectrum_in_arb_per_MeV")

# SPECIAL TESTS
# check for test case
if 'test' in input_dict.keys():
    # .. !TODO
    pass

# LOOP OVER INPUT FILES
print('\nWORK WITH DATA\n')
if input_dict["online"] == 'F':
    # DATA ANALYSIS
    for file in input_data:
        # Call analysis procedure
        img = analyze(file,input_dict)
        if img == False:
            continue
        # Call output procedures
        write_out(img)
elif input_dict["online"] == "T":
    # ONLINE DATA ANALYSIS
    help_message =  'In order to exit the online analysis, press and hold ctrl + C\n'+\
                    'To change the width of the lineout,   press and hold L\n'+\
                    'All analysis with the last selection, press and hold S\n'+\
                    'To abort fixed selection mode,        press and hold A\n'+\
                    'To repeat the last analysis,          press and hold R\n'
    try:
        print("WAIT FOR NEW DATA")
        func.MESSAGE_PRINT('run.py',help_message)
        do_use_same_lineout = None
        the_last_lineout = None
        while True:
            # get new data files
            input_data,old_data = load.next_datafiles(input_dict['data'],[script_path,work_path],old_data)
            # process data files if any
            for file in input_data:
                # call analysis procedure
                print('\n')
                if do_use_same_lineout == True:
                    img = analyze(file,input_dict,fixed_lineout=the_last_lineout)
                else:
                    img = analyze(file,input_dict)
                    the_last_lineout = img['lineout']
                print('\n')
                if img == False:
                    # remember file
                    old_data.append(file)
                    # go to next
                    continue
                # set switch after first lineout
                if do_use_same_lineout is None:
                    do_use_same_lineout = False
                # call output procedures
                write_out(img, silent = True)
                # remember file
                old_data.append(file)
                
            # wait and listen one second
            print('#', end="", flush=True)
            inp = func.FunctionThread(readchar.readkey)
            inp.start()
            time.sleep(1)
            if inp.result != None:
                if inp.result == "'r'" and len(old_data) >= 1:
                    print('\n')
                    if do_use_same_lineout == True:
                        img = analyze(old_data[-1],input_dict,fixed_lineout=the_last_lineout)
                    else:
                        img = analyze(old_data[-1],input_dict)
                        the_last_lineout = img['lineout']
                    print('\n')
                elif inp.result == "'l'":
                    print('\n\nREPLACE OLD LINEWIDTH')
                    try:
                        linewidth = int(input())
                    except:
                        func.MESSAGE_PRINT('run.py','Expected integer number, abort!')
                    print('\n')
                elif inp.result == "'s'":
                    print('\n\n')
                    if do_use_same_lineout is not None:
                        print('SELECT LAST LINEOUT AS FIXED POSITION')
                        do_use_same_lineout = True
                    else:
                        print('NO LAST LINEOUT AVAILABLE')
                    print('\n')
                elif inp.result == "'a'":
                    print('\n\n')
                    if do_use_same_lineout == True:
                        print('ABORT LAST LINEOUT POSITION')
                        do_use_same_lineout = False
                    elif do_use_same_lineout is None:
                        print('NO LAST LINEOUT AVAILABLE')
                    print('\n')
                elif inp.result == "'h'":
                    print('\n\n')
                    func.MESSAGE_PRINT('run.py',help_message)
                else:
                    print('\n\n')
                    func.MESSAGE_PRINT('run.py',inp.result+" is no valid input, for help press and hold H")
                    
            inp.raiseExc(Exception)
            del inp # play safe
            
    except (KeyboardInterrupt, SystemExit):
        func.MESSAGE_PRINT('run.py',"\nEXIT ONLINE ANALYSIS")   
    print("#")
else:
    func.ERROR_PRINT(1287)

# ==============================================================================
# TERMINATE
# ==============================================================================
# TAKE TIME
fin = time.time()

# PROMPT RUNTIME
print("\nRUNTIME = %3.1f s" % (fin-debut))

# SAY GOODBYE
func.GOODBYE()