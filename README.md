**pystan\_b**

Welcome to the pythonic spectral trace analysis for magnetic spectrometers.

# Table of Contents

- [Operator Manual](#operatormanual)
- [Installation of Runtime Environment](#runtimeenvironment)
- [Detailed Workflow](#workflow)

# Operator Manual <a id="operatormanual"></a>

The following section should introduce new users to the startup and handling of the program. For installation instructions, see [this section](#runtimeenvironment).

## Startup

*pySTAN\_b* is a program that works from the comand prompt. In Windows, search the *Anaconda prompt* in the start menu and run it. In POSIX systems, open a terminal. To enter the runtime environment type

```
conda activate pystan_b
```

and confirm with *Enter*.

The script `pystan_b/run.py` takes information from input files during its run. To run the script, type

```
python run.py path/to/input_file.csv
```

and launch with *Enter*. The test run makes use of the example input file `pystan_b/input_file.csv`. To get going, you can copy and modify this file. All fields of this file are explained in [this section](#workflow).

After a successfull launch, the program will promt first the current reference image, then the current reference for the zero-point. In order to further advance, close the prompts successively. The display is performed to remind you of the current settings in the input file, if you want to exit the program and change the settings press and hold `ctrl + c`. 

Text in the comand prompt throughoutly gives advice on required actions or information on the status of analysis.

## First Dialogue

The first user input concers the selection of the zero-point. The program prompts the zero-reference and the operator is asked to slect the zero point of the coordinate system of the spectrometer screen. This point is used in all the coming steps of data analysis.

## Modes of Operation

The script can be used as online analysis tool during campaigns or as script for data analysis. Wehn used online, the script will prompt freshly arriving files to the operator who then can analyse them. When used as analysis script, all files in the selected folder will be iterated.

## Data Analysis

The program prompts the measured signal and lets the operator draw a line, following the spectral trace. After confirmation by `c`, the program will calculate the spectrum.

## Quick Results

All intermediate results and the final spectrum of the last analysis can be displayed in a browser by opening the file `pystan_b/utils/analysis_control.html`. The page is setup to refresh automatically in regular intervalls.


# Installation of Runtime Environment <a id="runtimeenvironment"></a>

The python script is best cloned from the source (not downloaded) and executed within an Anaconda environment (not within your standard python environment). The first is usefull for uncomplicated updates and an easy contribution to the project, the later is ought to protect your habitual working environments and keep them clean. The installation and execution procedures are kept simple in order to allow users to deploy this toolset who are not familiar to python.

## GIT Environment

*pySTAN\_b* is a collaborative project, we would like to encourage that you get started with [git](https://git-scm.com/downloads) in order to join the developpers. Anyway, please use to following method to install the relevant scripts on your computer: 

1. make sure git is installed
  A. in Windows search for `gitbash` and install [git for windows](https://gitforwindows.org/) if you can not locate it, 
  B. in POSIX systems try `which git` and install [git](https://git-scm.com/downloads) if no answer is given to you; 
2. copy the project files from the internet onto your computer
  1. open *gitbash* or *terminal* respectively,
  2. navigate to a location that you want to use as working directory - e.g. in Windows `C:\working\directory` or in POSIX sustems `/working/directory`, 
  3. type `git clone https://memx@bitbucket.org/clpu/pystan_b.git` and download the project files by executing with `enter` - here `https://memx@bitbucket.org/clpu/pystan_b.git` is the default repository location which you can change if your institution has another version of the code on some server - or if you have a version of the code in a file;
3. if you open a filemanager, you can take a look in the created directory `pystan_b` - it should be populated with some files and directories.

## Python Environment

We recommend you to set up a clean python environment with [Anaconda](https://www.anaconda.com/distribution/). Set up Anaconda (version 3) by following the installation guides on the project's web page. If you do not like to work with Anaconda, please find the details of used packages in `pystan_b/pystan_b.yml`. Depending on the operating system, the next setup steps differ.

### Windows
Open Anaconda navigator, navigate to *Environments* and click on the play button behind *base* to launch the anaconda prompt. Navigate to the `pystan_b` directory, then execute the following installation instructions (answer with yes, if asked for permission to install packages)
```
(base) C:\working\directory\pystan_b> conda env create -f pystan_b.yml
(base) C:\working\directory\pystan_b> conda activate pystan_b
(pyridi) C:\working\directory\pystan_b> 
```

### Linux
```
(base) user@computer:/working/directory/pystan_b$ conda env create -f pystan_b.yml
(base) user@computer:/working/directory/pystan_b$ conda activate pystan_b
(pyridi) user@computer:/working/directory/pystan_b$
```

## Test Run

Perform a first run as test to ensure that the environment works fine. Execute
```
python run.py
```

## Known Errors and Troubleshooting

### OpenCV

Often the module *CV2* is not recognized, manifested with the error
```
    import cv2
ImportError: DLL load failed: The specified module could not be found.
```
then do
```
conda update -n base -c defaults conda
conda install -c fastai opencv-python-headless
conda uninstall opencv
conda uninstall opencv-python-headless
conda install opencv
```
which shakes a bit the faulty installation and eventually brings you CV2 with GUI support.

Another error with openCV is due to to a incompatible compile or a double installation. If an error like the following occures:
```
(-2:Unspecified error) The function is not implemented. Rebuild the library with Windows, GTK+ 2.x or Cocoa support. If you are on Ubuntu or Debian, install libgtk2.0-dev and pkg-config, then re-run cmake or configure script in function 'cvNamedWindow'
```
then do the suggested action in Ubuntu or Debian. Working on Windows, do
```
conda uninstall opencv
pip install opencv-python
pip install imutils
```
which changes the conda version for the (hopefully) working pip version.

---

# Detailed Workflow <a id="workflow"></a>

The script `pystan_b/run.py` takes information from input files during its run. The test run makes use of the example input file `pystan_b/input_file.csv`. To get going, you can copy and modify this file to start with your own setup: e.g. the folder `pystan_b/usr` is not used for project development and could be ideal to serve as your user directory. The comma separated input file contains one input parameter per row, and each row contains

1. a *key* that referrs to the parameter name,
2. the *value* that is used as constant for this parameter,
3. optionally a *help* string to give room for comments.


The full namespace of input parameters lets you define

- &cross; `test` of type string which defines if the run is a test run, accepted values are:
  a. `T` if yes,
  b. `F` if not,
  defaults to `F`;
- &cross; `online` of type string, accepted values are:
  a. `T` if the script should run as online analysis,
  b. `F` if this value is not known,
  defaults to `F`, for details see the section *Modes of Operation* in *Additional Information*;
- &cross; `imaged_side` of type string which indicates from which side the screen is imaged, accepted values are:
  a. `back` if the camera faces the back side of the screen with respect to the impacting projectiles,
  b. `front` if the camera faces the front side respectively;
- &cross; `pixel_to_mm` of type float which defines the spatial callibration of the imaging system, mapping the size of a recorded squared pixel to its spatial width (projected to the imaged plane);
- &cross; `data` of type string which defines the directory containing data files or a specific data file, the path is first understood as relative path with root `pystan_b/` and in case of no success as absolute path;
- &cross; `reference` of type string which defines a specific reference file in experimental conditions but without actual measurement trace, the path is first understood as relative path with root `pystan_b/` and in case of no success as absolute path;
- &cross; `zero_calibration` of type string which defines a specific reference file on which the spatial zero is visible (usually the undeflected direction of particles), the path is first understood as relative path with root `pystan_b/` and in case of no success as absolute path;
- &cross; `channel` of type string which defines the detector channel which should be used for analysis, accepted values are:
  a. `R` the red channel,
  b. `G` the green channel,
  c. `B` the blue channel,
  d. `A` the average of all available channels;
- &cross; `linewidth` of integer which defines the transverse width of the line drawn to extract the spectral trace from the data, in units of pixel;
- &cross; `projectiles` of type string which defines the projectile species, for the catalogue of available species see the library in `LIB/species.csv`;
- &cross; `energy_calibration` of type string which defines how the energy calibration should be performed, either:
  a. based on a specific data file, the field then should contain a path which is first understood as relative path with root `pystan_b/` and in case of no success as absolute path,
  b. based on the analytical solution with regard to a specific magnet, the field then should contain the key `analytic`;
- &cross; `magnet_key` of type string which defines the magnet which is is used in the spectrometer, this information is needed when `energy_calibration` is set to `analytic`, for the catalogue of available magnets see the magnet library in `LIB/magnets.csv`;
- &cross; `magnet_dist` of type float which defines the distance between the centre plane of the magnet which is is used in the spectrometer and the imaged screen, this information is needed when `energy_calibration` is set to `analytic`;
- &cross; `output_directory` of type string which defines the directory for output files, the path is understood as absolute path and defaults to `usr/out`;
- &cross; `plot_spectrum_xaxis` of type string which defines the axis type of the x-axis for the automatic spectrum plotter, either:
  a. `log` logarithmic,
  b. `linear` for linear,
  defaults to `symlog`;
- &cross; `plot_spectrum_xaxis_max` of type float which defines the upper bound of the x-axis for the automatic spectrum plotter.

---

# Additional Information



---

# Milestones

- &check; setup project repository
- &check; create test environment towards runtime test
- &check; online analysis during a test campaign
- &cross; implement feedback loop for individual anaysis (do again if not happy)
