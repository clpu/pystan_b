﻿# -*- coding: utf-8 -*-
"""
This is the physics module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload

import numpy as np

import skimage.restoration as ski_r
import scipy.integrate as sci_int
import scipy.optimize as sci_opt
import scipy.interpolate as sci_inter
import cv2

# EXTEND PATH
# ..

# INTERNAL
if "coucou_constants" not in globals():
    from constants import *   # !! import all global constants from LIB/constants.py
elif globals()['coucou_constants'] == False:
    from constants import *   # !! import all global constants from LIB/constants.py

import functions as func  # !! functional routines applicable to many cases
import image as image     # !! image routines applicable to many cases
import plotter as plotter # !! plotter routines applicable to many cases

import loader as load            # !! routines fetching input data from SCR/loader.py

# RELOAD
reload(func)
reload(image)
reload(plotter)

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================
# typical values
typical_momentum = np.sqrt(2. * 1. * GeV_to_J * m_u)

# ==============================================================================
# HELPER FUNCTIONS
# ==============================================================================
# .. 

# ==============================================================================
# FUNCTIONS TO MANAGE PHYSICS
# ==============================================================================
# relativistic gamma factor
def gamma(v):
    return 1./np.sqrt(1. - v**2 / speed_of_light**2)
    
def v_of_p(p,m):
    return speed_of_light * p / np.sqrt((m*speed_of_light)**2 + p**2)
    
# relativistic kinetic energy
def E_k_rel(v,m0):
    return (gamma(v) - 1.) * m0 * speed_of_light**2

# ==============================================================================
# FUNCTIONS TO MANAGE DATA
# ==============================================================================

def give_energyspectrum(input_dict,pixel_axis,lineout,input_file):
    '''
    TRANSFORM PIXEL POSITIONS IN ENERGY AXIS AND HISTOGRAM IN SPECTRUM
    INPUT_DICT :: USER INPUT_DICT
    ...
    '''
    print('CALCULATE ENERGY SPECTRUM')
    # check user input data for completeness and consistency
    if load.USE_INPUT_KEY('physics.py','energy_calibration',input_dict) and\
      load.USE_INPUT_KEY('physics.py','imaged_side',input_dict) and\
      load.USE_INPUT_KEY('physics.py','pixel_to_mm',input_dict):
        if input_dict['energy_calibration'] == 'analytic':
            if load.USE_INPUT_KEY('physics.py','magnet_key',input_dict) and \
              load.USE_INPUT_KEY('physics.py','magnet_dist',input_dict) and \
              load.USE_INPUT_KEY('physics.py','projectiles',input_dict):
              pass
        else:
            func.MESSAGE_PRINT('physics.py',"Only analytic way is implemented, speak with admin.")
            func.ERROR_PRINT(1169)
    # magnification
    if not isinstance(input_dict['pixel_to_mm'],float) and \
      not isinstance(input_dict['pixel_to_mm'],int):
        func.MESSAGE_PRINT('run.py',"No valid value for 'pixel_to_mm'!")
        func.ERROR_PRINT(13)
    # pixel to energy conversion
    if input_dict['imaged_side'] == 'front':
        mm_axis = pixel_axis * input_dict['pixel_to_mm']
    else:
        mm_axis = - pixel_axis * input_dict['pixel_to_mm']
    if input_dict["image_inverted_x"] == 'T':
        mm_axis = - mm_axis
        
    # calculate energy axis
    energy_axis,energy_positions = deflection_to_energy(mm_axis,\
                                       magnets[input_dict['magnet_key']]['field_amplitude_in_T'],\
                                       species[input_dict['projectiles']]['charge_in_C'],\
                                       species[input_dict['projectiles']]['mass_in_kg'],\
                                       magnets[input_dict['magnet_key']]['effective_length_in_mm']/2. + input_dict['magnet_dist'],\
                                       magnets[input_dict['magnet_key']]['effective_length_in_mm'])
    # recitfy plot style
    if 'plot_spectrum_xaxis' in input_dict.keys():
        plot_spectrum_xaxis = input_dict['plot_spectrum_xaxis']
    else:
        plot_spectrum_xaxis = plotter.plot_spectrum_xaxis_default
    if plot_spectrum_xaxis != 'log' and plot_spectrum_xaxis != 'linear' and plot_spectrum_xaxis != 'symlog':
        plot_spectrum_xaxis = plotter.plot_spectrum_xaxis_default
    
    if 'plot_spectrum_xaxis_max' in input_dict.keys():
        plot_spectrum_xaxis_max = input_dict['plot_spectrum_xaxis_max']
    else:
        plot_spectrum_xaxis_max = plotter.plot_spectrum_xaxis_max_default
    
    # plot axis conversion
    print("> PLOT CONVERSION OF SCREEN POSITION TO ENERGY")
    plotter.plot2D(mm_axis[energy_positions],energy_axis/MeV_to_J, input_dict['temp_dir']+os.path.sep+"energy_of_screenposition.png", x_label = "Position / [mm]", y_label = "Energy / [MeV]", y_scale = 'log', silent = True)
    # space to energy derivative
    mm_to_J = np.gradient(mm_axis[energy_positions],energy_axis)
    # plot axis derivative
    plotter.plot2D(mm_axis[energy_positions],mm_to_J*MeV_to_J, input_dict['temp_dir']+os.path.sep+"dxdE-1_of_screenposition.png", x_label = "Position / [mm]", y_label = "dx/dE / [mm/MeV]", silent = True)
    # prepare spectrum
    spectrum = lineout[energy_positions] * input_dict['pixel_to_mm'] * mm_to_J
    # plot spectrum
    plotter.plot2D(energy_axis/MeV_to_J,spectrum*MeV_to_J, input_dict['temp_dir']+os.path.sep+"spectrum_dNdE-1.png", x_label = "Energy / [MeV]", y_label = "dN/dE / [const #/MeV]", x_scale=plot_spectrum_xaxis, y_scale = 'symlog', silent = True, x_max = plot_spectrum_xaxis_max)
    if 'output_directory' in input_dict.keys():
        plotter.plot2D(energy_axis/MeV_to_J,spectrum*MeV_to_J, input_dict['output_directory']+os.path.sep+func.STRIP_EXTENSION(os.path.basename(input_file))+"_spectrum_dNdE-1.png", x_label = "Energy / [MeV]", y_label = "dN/dE / [const #/MeV]", x_scale=plot_spectrum_xaxis, y_scale = 'symlog', silent = True, x_max = plot_spectrum_xaxis_max)
    # output
    return {'energy_axis':energy_axis,'spectrum':spectrum}
    
def deflection_to_energy(position_in_mm,B0,qp,mp,drift,magnet_length):
    """
    FROM RELATIVE POSITION ON SCREEN TO ENERGY VALUE
    POSITION_TO_MM :: SCREEN POSITION IN MM
    B0             :: MAGNETIC FIELD AMPLITUDE IN T
    QP             :: PROJECTILE CHARGE IN C
    MP             :: PROJECTILE MASS in KG
    DRIFT          :: DRIFT DISTANCE BETWEEN MAGNET AND SCREEN IN MM
    MAGNET_LENGTH  :: LONGITUDINAL EXTEND OF MAGNETIC FIELD
    """
    # calculate dimensionless field factor
    pA = (magnet_length * mm_to_m) * B0 * qp
    v_A = v_of_p(pA,mp)
    E_A = E_k_rel(v_A,mp)
    # calculate minimum energy
    v_min = v_A
    E_min = E_A
    print("> SPECTROMETER")
    print("  MAGNET LENGTH  IS "+str(magnet_length)+" mm")
    print("  MAGNETIC FIELD IS "+str(B0)+" T")
    print("  PROJECTILE CHARGE "+str(qp)+" C")
    print("  PROJECTILE MASS   "+str(mp)+" kg")
    print("  MINIMUM ENERGY IS "+str(E_min/GeV_to_J)+" GeV")
    print("  (q*B) MOMENTUM IS "+str(pA)+" kg m / s")
    # conclude on sign of position which is consistent with projectile charge and check position data
    position_in_mm = np.array(position_in_mm)
    if pA > 0.:
        sign = -1.
        positions = np.where(position_in_mm < 0.)
    else:
        sign = 1.
        positions = np.where(position_in_mm > 0.)
    # OLD SOLUTION
    # define newtons system for construction of roots with normalized variable P = pA/p0,
    #  which ranges in the intervall (-1,1)
    #  where p0 is the searched momentum that corresponds to a screen position,
    #  with F'= f:
    #def F(P):
    #    if np.any(np.abs(P) > 1):
    #        coordinates = np.where(np.abs(P) > 1)
    #        P[coordinates] = 0.99 * np.sign(P[coordinates]) # manual bracketing of newton
    #    if position_in_mm.size == 1:
    #        return drift/position_in_mm * np.tan(np.arcsin(P)/2.) + magnet_length/position_in_mm * np.tan(np.arcsin(P)) + 1.
    #    else:
    #        return drift/position_in_mm[positions] * np.tan(np.arcsin(P)/2.) + magnet_length/position_in_mm[positions] * np.tan(np.arcsin(P)) + 1.
    #def f(P):
    #    if np.any(np.abs(P) > 1):
    #        coordinates = np.where(np.abs(P) > 1)
    #        P[coordinates] = 0.99 * np.sign(P[coordinates]) # manual bracketing of newton
    #    if position_in_mm.size == 1:
    #        return - (drift/position_in_mm * (P**2-1.) / np.cos(np.arcsin(P)/2.)**2 - 2. * magnet_length/position_in_mm) / (2. * np.sqrt((1.-P**2)**3))
    #    else:
    #        return - (drift/position_in_mm[positions] * (P**2-1.) / np.cos(np.arcsin(P)/2.)**2 - 2. * magnet_length/position_in_mm[positions]) / (2. * np.sqrt((1.-P**2)**3))
    #if position_in_mm.size == 1:
    #    momentum_P  = sci_opt.newton(F,0.5*sign,fprime = f) # for debugging: disp=True,full_output=True
    #else:
    #    momentum_P  = sci_opt.newton(F,0.5*sign*np.ones(position_in_mm[positions].shape),fprime = f) # for debugging: disp=True,full_output=True
    # END OLD SOLUTION
    # define a function for construction with normalized variable P = pA/p0,
    #  which ranges in the intervall (-1,1)
    #  where p0 is the searched momentum that corresponds to a screen position
    def screen_position(P):
        return - (drift * np.tan(np.arcsin(P)/2.) + magnet_length * np.tan(np.arcsin(P)))
    test_P = np.linspace(-0.99,0.99,num=10000)
    screen_position_of_test_P = screen_position(test_P)
    P_of_screen_position = sci_inter.interp1d(screen_position_of_test_P,test_P, fill_value='extrapolate')
    if position_in_mm.size == 1:
        momentum_P  = P_of_screen_position(position_in_mm)
    else:
        momentum_P  = P_of_screen_position(position_in_mm[positions])
    # extract momentum and enery from results
    momentum_p0 = pA/momentum_P
    energy_E0 = E_k_rel(v_of_p(momentum_p0,mp),mp)
    if isinstance(momentum_p0,float):
        print("  IMAGE MOMENTUM IS "+str(momentum_p0)+" kg m / s")
        print("  ENERGY ON SCREEN  "+str(energy_E0/GeV_to_J)+" GeV")
    else:
        print("  CALCULATED ARRAY OF IMAGED MOMETA")
    
    # return
    return energy_E0,positions