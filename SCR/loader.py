﻿# -*- coding: utf-8 -*-
"""
This is the loader module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload

import os
import sys
import time

import numpy as np
import cv2

# EXTEND PATH
# ..

# INTERNAL
import functions as func  # !! functional routines applicable to many cases
import image as image

# RELOAD
reload(func)
reload(image)

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================
# timout for file checks
global_timeout_loops = 1

# ==============================================================================
# FUNCTIONS TO MANAGE DATA IMPORT
# ==============================================================================
def parse_inputfile(path,lookup_table):
    """
    READ INPUT CSV FILE TO INPUT DICT
    PATH            :: LOOKUP PATH
    LOOKUP_TABLE    :: SEARCH FILES HERE
    """
    print('\nPARSE INPUTFILE')
    input_dict = {}
    if path == None:
        print(" > NO FILE SPECIFIED")
        print("   please specify an input file by calling 'python run.py input.csv'")
        print("   perform run using 'input_file.csv'")
        path = 'input_file.csv'
    
    path = path.replace('/',os.path.sep)
    if not os.path.isfile(path):
        print(' > DO NOT FIND '+path)
        for lookup in lookup_table:
            if not os.path.isfile(lookup+os.path.sep+path):
                print(' > DO NOT FIND '+lookup+os.path.sep+path)
            else:
                path = lookup+os.path.sep+path
                break
    
    input_dict = func.READ_CSV(path, dictcols=1, header = True)
    
    for key in input_dict.keys():
        input_dict[key] = input_dict[key]['value']
        if isinstance(input_dict[key],str):
            input_dict[key] = input_dict[key].replace('/',os.path.sep)
    
    return input_dict

def USE_INPUT_KEY(fname,key,dic):
    """
    HARD CHECK OF APPEARANCE OF KEY IN INPUT DICT
    FNAME  :: CALLING SCRIPT FOR BACKTRACE
    KEY    :: LOOKUP KEY
    DIC    :: SEARCH LOOKUP HERE
    """
    if key not in dic:
        func.MESSAGE_PRINT(fname,"The input field for '"+key+"' is empty!")
        func.ERROR_PRINT(1169)
    return True
        
def parse_datafiles(path,lookup_table,*args,**kwargs):
    """
    READ DATA FILE(S)
    PATH            :: LOOKUP PATH
    LOOKUP_TABLE    :: SEARCH PATH HERE
    """
    silent = kwargs.get('silent', False)
    if not silent: print('\nPARSE DATA')
    experiment = []
    try:
        if path != None:
            if not os.path.isfile(path):
                if not silent: print(" > DO NOT FIND FILE '"+path+"'")
                for lookup in lookup_table:
                    if not os.path.isfile(lookup+os.path.sep+path):
                        if not silent: print(" > DO NOT FIND FILE '"+lookup+os.path.sep+path+"'")
                    else:
                        experiment.append(lookup+os.path.sep+path)
                        break
            else:
                experiment.append(path)
            if len(experiment) == 0:
                if not os.path.isdir(path):
                    if not silent: print(" > DO NOT FIND DIRECTORY '"+path+"'")
                    for lookup in lookup_table:
                        if not os.path.isdir(lookup+os.path.sep+path):
                            if not silent: print(" > DO NOT FIND DIRECTORY '"+lookup+os.path.sep+path+"'")
                        else:
                            files = os.listdir(lookup+os.path.sep+path)
                            for file in files:
                                experiment.append(lookup+os.path.sep+path+os.path.sep+file)
                            break
                else:
                    files = os.listdir(path)
                    for file in files:
                        experiment.append(path+os.path.sep+file)
            
            if len(experiment) != 0 and not silent : print(' > FOUND ')
            for file in experiment:
                if not silent: print("   '"+file+"'")
        
        return experiment
        
    except:
        func.ERROR_PRINT(1287)
        
def next_datafiles(path,lookup_table,seen_datafiles):
    """
    READ NEXT DATA FILE(S)
    PATH            :: LOOKUP PATH
    LOOKUP_TABLE    :: SEARCH PATH HERE
    SEEN_DATAFILES  :: EXCLUDE THIS FROM RETURN
    """
    tmp_files = parse_datafiles(path,lookup_table,silent=True)
    old_files = seen_datafiles
    for item in seen_datafiles:
        try:
            tmp_files.remove(item)
        except:
            # files were deleted or removed
            old_files.remove(item)
    new_files = tmp_files
    return new_files,old_files
        
def parse_reference(path,lookup_table):
    """
    READ DATA FILE(S)
    PATH            :: LOOKUP PATH
    LOOKUP_TABLE    :: SEARCH PATH HERE
    """
    print('\nPARSE REFERENCE')
    experiment = ''
    try:
        if path != None:
            if not os.path.isfile(path):
                print(" > DO NOT FIND FILE '"+path+"'")
                for lookup in lookup_table:
                    if not os.path.isfile(lookup+os.path.sep+path):
                        print(" > DO NOT FIND FILE '"+lookup+os.path.sep+path+"'")
                    else:
                        experiment = lookup+os.path.sep+path
                        break
            else:
                experiment = path
            
            if len(experiment) != 0: print(' > FOUND ')
            print("   '"+experiment+"'")
        
        return experiment
        
    except:
        func.ERROR_PRINT(1287)
        
def dataset(sourcefile):
    """
    READ AND SORT INPUT DATA FILES FOR DATA ANALYSIS
    REFERENCES_DICT :: DICTIONARY SPECIFYING SOURCE FILES
    """
    global global_timeout_loops
    print(' > LOAD DATASET')
    output_dict = {}
    try:
        # LOAD IMAGE PATH
        output_dict['source'] = sourcefile
        
        # CHECK IF IMAGE IS STATIC OR BUILDING
        filesize_was = os.stat(sourcefile).st_size
        time.sleep(global_timeout_loops)
        filesize_is  = os.stat(sourcefile).st_size
        while filesize_was != filesize_is:
            filesize_was = filesize_is
            filesize_is = os.stat(sourcefile).st_size
            time.sleep(global_timeout_loops)
        
        # LOAD IMAGE
        output_dict['image'] = image.IMREAD(sourcefile)
        
        if len(output_dict['image'].shape) == 3:
            output_dict['height'], output_dict['width'], output_dict['channels'] = output_dict['image'].shape
        elif len(output_dict['image'].shape) == 2:
            output_dict['height'], output_dict['width'] = output_dict['image'].shape
            output_dict['channels'] = 1
        else:
            func.MESSAGE_PRINT('loader.py',"The input image has too much dimensions!")
            func.ERROR_PRINT(677)
        
        if output_dict['image'].dtype == 'uint8':
            output_dict['depth'] = int(2.**8)
        
        # SELECT ROI
        # !TODO and in own function
        # output_dict['cropped'] = image.INTERACTIVE_CROP(sourcefile,adjust = True)

        # EXIT
        return output_dict
    except:
        func.ERROR_PRINT(1287)