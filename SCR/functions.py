﻿# -*- coding: utf-8 -*-
"""
This is the function module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
import inspect

import os
import sys
import time
import threading

import numpy as np
import cv2
import ctypes

from inspect import getsourcefile
from collections import namedtuple as nt

import tifffile as tiff # !! DOC: https://www.lfd.uci.edu/~gohlke/code/tifffile.py.html

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================

invalid_int = -9999

# ==============================================================================
# CLASSES TO MANAGE RUNTIME ENVIRONMENT
# ==============================================================================

class Alarm(threading.Thread):
    def __init__(self, timeout, **kwargs):
        threading.Thread.__init__ (self)
        self.timeout = timeout
        self.ringing = kwargs.get('ringing', None)
        self.result = []
        self.setDaemon(True)
    def run(self):
        time.sleep(self.timeout)
        if self.ringing == None:
            os._exit(1)
        else:
            self.result = self.ringing

def _async_raise(tid, exctype):
    '''
    Raises an exception in the threads with id tid 
    https://stackoverflow.com/a/325528
    '''
    if not inspect.isclass(exctype):
        raise TypeError("Only types can be raised (not instances)")
    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_long(tid),
                                                     ctypes.py_object(exctype))
    if res == 0:
        raise ValueError("invalid thread id")
    elif res != 1:
        # "if it returns a number greater than one, you're in trouble,
        # and you should call it again with exc=NULL to revert the effect"
        ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_long(tid), None)
        raise SystemError("PyThreadState_SetAsyncExc failed")
            
class FunctionThread(threading.Thread):
    def __init__(self, function, **kwargs):
        threading.Thread.__init__ (self)
        self.function = function
        self.result = None
        self.setDaemon(True)
        self._running = True
    def _get_my_tid(self):
        """
        determines this (self's) thread id
        https://stackoverflow.com/a/325528
        CAREFUL: this function is executed in the context of the caller
        thread, to get the identity of the thread represented by this
        instance.
        """
        if not self.isAlive():
            raise threading.ThreadError("the thread is not active")

        # do we have it cached?
        if hasattr(self, "_thread_id"):
            return self._thread_id

        # no, look for it in the _active dict
        for tid, tobj in threading._active.items():
            if tobj is self:
                self._thread_id = tid
                return tid
    def raiseExc(self, exctype):
        """Raises the given exception type in the context of this thread.
        https://stackoverflow.com/a/325528

        If the thread is busy in a system call (time.sleep(),
        socket.accept(), ...), the exception is simply ignored.

        If you are sure that your exception should terminate the thread,
        one way to ensure that it works is:

            t = ThreadWithExc( ... )
            ...
            t.raiseExc( SomeException )
            while t.isAlive():
                time.sleep( 0.1 )
                t.raiseExc( SomeException )

        If the exception is to be caught by the thread, you need a way to
        check that your thread has caught it.

        CAREFUL: this function is executed in the context of the
        caller thread, to raise an exception in the context of the
        thread represented by this instance.
        """
        try:
            _async_raise( self._get_my_tid(), exctype )
        except:
            print("  THE INACTIVE THREAD CAN NOT BE CLOSED\n")
    def run(self):
        try:
            self.result = repr(self.function())
        except:
            pass
        
# ==============================================================================
# FUNCTIONS TO MANAGE RUNTIME ENVIRONMENT
# ==============================================================================
def COMMAND_LINE(expect_list):

    #SEARCH FOR COMMAND LINE INPUT
    arg_names = expect_list
    args = dict(zip(arg_names, sys.argv))

    Arg_list = nt('Arg_list', arg_names)
    args = Arg_list(*(args.get(arg, None) for arg in arg_names))
    
    return args
    
def ENVIRONMENT(**kwargs):

    # PRINT PATH OF MAIN SCRIPT
    root = kwargs.get('root', None)
    if root != None:
        print("root directory           : " + root)

    # GET PATH OF EXECUTED SCRIPT AND WORKING DIRECTORY
    try:
        scriptpath = os.path.dirname(os.path.abspath(getsourcefile(lambda:0)))
        print("script directory         : " + scriptpath)

        workpath = os.path.abspath(os.getcwd())
        print("shell execution directory: " + workpath)
    except:
        ERROR_PRINT(1287)
    
    return scriptpath,workpath

def HOMEBASE(addr):
    
    if '~' in addr:
        home = os.path.expanduser("~")
        addr = home + addr.strip('~')
        
    return addr
    
def there_from_here(rel_there,abs_here):

    here_list = HOMEBASE(abs_here).split(os.path.sep)
    there_list = rel_there.split(os.path.sep)
    
    for item in there_list:
        if item == '..':
            here_list.pop(-1)
        else:
            break
    
    path_list = here_list+there_list
    
    return (os.path.sep).join(path_list)
    
def STRIP_EXTENSION(filename):
    root = ((filename[::-1]).split('.',1)[1])[::-1]
    return root
    
def GIVE_EXTENSION(filename):
    extension = filename.split(".")[1]
    return extension
    
def GIVEINVALIDINT():
    
    return invalid_int
    
def CHECKINT(integer):

    if integer == invalid_int:
        check = False
    else:
        check = True
    
    return check
    
def POST_LTR(arraylike,destination):
    # UNDERSTAND DATA STRUCTURE #TODO CURRENTLY MAX 3 LEVELS
    arraylike = np.array(arraylike)
    structure = arraylike.shape
    # WRITE OUT
    with open(destination, 'w') as f:
        for yitem in arraylike:
            for xitem in yitem:
                if isinstance(xitem,list):
                    xitem = '['+(" ".join(xitem))+']'
                f.write("%s " % xitem)
            f.write("\n")
            
def READ_LTR(path):
    with open(path, 'r') as f:
        letter = f.readlines()
    arraylike = []
    for line in range(len(letter)):
        letter[line] = letter[line].strip()
        if "[" in letter[line]:
            letter[line] = letter[line].strip('[]')
            letter[line] = letter[line].split('] [') #TODO , >
            if len(letter[line]) != 0:
                for element in range(len(letter[line])):
                    letter[line][element] = letter[line][element].split()
        else:
            letter[line] = letter[line].split()
        arraylike.append(letter[line])
    return np.array(arraylike)

def READ_CSV(path,dictcols=1, header = False): # TODO ADD ARGUMENT TO SPECIFY VAR DATA-TYPE (FLOAT,STR,MIXED) FOR PARSER (*)
    """
    READ CSV FILE TO DICT
    PATH               :: LOOKUP PATH
    DICTCOLS, OPTIONAL :: ADD FIRST N COLS TO DICTKEYS DICT{COL_1},..,{COL_N},{(ROW_N+1:) :: VALUE}
    HEADER, OPTIONAL   :: PRESENCE OF HEADER
    """
    if header:
        try:
            #print(path)
            with open(path,'r') as openfile:
                csv_dict,row_dict = {},{}
                head = openfile.readline().replace('\n', '').split(',')[dictcols:]
                for col in head:
                    row_dict[col] = head.index(col) + dictcols
                for row in openfile :
                    new_dict = {}
                    # Retrive Data
                    row_array = row.strip().split(',')
                    data_dict = {}
                    for col in head:
                        try:
                            data_dict[col] = float(row_array[row_dict[col]]) # (*)
                        except:
                            data_dict[col] = row_array[row_dict[col]] # (*)
                    new_dict = data_dict.copy()
                    # Retrive Keys
                    row_keys = row_array[:dictcols]
                    for key in reversed(row_keys):
                        new_dict = {key : new_dict.copy()}
                    # UPDATE DICT
                    csv_dict = MERGE_DICT(csv_dict,new_dict)
                    #csv_dict.update(new_dict)
   
                return csv_dict
        except:
            ERROR_PRINT(2)
    else:
        ERROR_PRINT(13)

def MERGE_DICT(dict1, dict2):
    dict3 = {**dict1, **dict2}
    for key, value in dict3.items():
        if key in dict1 and key in dict2:
            if isinstance(dict1[key],dict) and isinstance(dict2[key],dict):
                # DICT TO DICT MERGE
                dict3[key] = MERGE_DICT(dict1[key],dict2[key])
            else:
                # VALUE TO ARRAY MERGE
                if isinstance(dict1[key],list):
                    dict3[key] = [value]
                    for arr_val in dict1[key]:
                        dict3[key].append(arr_val)
                else:
                    dict3[key] = [value , dict1[key]]
    return dict3
    
def find_nearest(array, value):
    """
    https://stackoverflow.com/a/2566508
    big fat warning: if your data contains np.nan, those points will always come out as nearest
    """
    array = np.asarray(array)
    arti_max = np.nanmax(array[np.isfinite(array)] - value)
    difference = np.abs(array - value)
    no_nan = np.where(difference == np.nan, arti_max, difference)
    idx = no_nan.argmin()
    return idx,array[idx]
    
def point_in_polygon(polygon, point):
    # https://www.algorithms-and-technologies.com/point_in_polygon/python 16/09/2020
    """
    Raycasting Algorithm to find out whether a point is in a given polygon.
    Performs the even-odd-rule Algorithm to find out whether a point is in a given polygon.
    This runs in O(n) where n is the number of edges of the polygon.
     *
    :param polygon: an array representation of the polygon where polygon[i][0] is the x Value of the i-th point and polygon[i][1] is the y Value.
    :param point:   an array representation of the point where point[0] is its x Value and point[1] is its y Value
    :return: whether the point is in the polygon (not on the edge, just turn < into <= and > into >= for that)
    """

    # A point is in a polygon if a line from the point to infinity crosses the polygon an odd number of times
    odd = False
    # For each edge (In this case for each point of the polygon and the previous one)
    i = 0
    j = len(polygon) - 1
    while i < len(polygon) - 1:
        i = i + 1
        # If a line from the point into infinity crosses this edge
        # One point needs to be above, one below our y coordinate
        # ...and the edge doesn't cross our Y corrdinate before our x coordinate (but between our x coordinate and infinity)

        if (((polygon[i][1] > point[1]) != (polygon[j][1] > point[1])) and (point[0] < (
                (polygon[j][0] - polygon[i][0]) * (point[1] - polygon[i][1]) / (polygon[j][1] - polygon[i][1])) +
                                                                            polygon[i][0])):
            # Invert odd
            odd = not odd
        j = i
    # If the number of crossings was odd, the point is in the polygon
    return odd
    
def stdinWait(text, function, default, time, timeoutDisplay = None, *args, **kwargs):
    def interrupt(signum, frame):
        raise Exception("")
    signal.signal(signal.SIGALARM, interrupt)
    signal.alarm(time) # sets timeout
    timeout = False
    try:
        inp = function(*args)
        signal.alarm(0)
        timeout = False
    except (KeyboardInterrupt):
        printInterrupt = kwargs.get("printInterrupt", True)
        if printInterrupt:
            print("Keyboard interrupt")
        timeout = True # Do this so you don't mistakenly get input when there is none
        inp = default
    except:
        timeout = True
        if not timeoutDisplay is None:
            print(timeoutDisplay)
        signal.alarm(0)
        inp = default
    return inp, timeout

def MESSAGE_PRINT(fname,*messages):
    string = " > "+fname+" wants to let you know something:"
    for message in messages:
        string = string + "\n   " + message
    print(string)
    return True

def ERROR_PRINT(eid):
    # ID   :: LABEL
    #    0 :: ERROR_DIVISION_BY_ZERO
    #         = The system cannot divide by zero.
    #    2 :: ERROR_FILE_NOT_FOUND
    #         = The system cannot find the file specified.
    #    5 :: ERROR_ACCESS_DENIED
    #         = Access is denied.
    #   13 :: ERROR_INVALID_DATA
    #         = The data is invalid.
    #  161 :: ERROR_BAD_PATHNAME
    #         = The specified path is invalid.
    #  232 :: ERROR_NO_DATA
    #         = The pipe is being closed.
    #  677 :: ERROR_EXTRANEOUS_INFORMATION
    #         = Too Much Information.
    # 1160 :: ERROR_SOURCE_ELEMENT_EMPTY        
    #         = The indicated source element has no media.
    # 1169 :: ERROR_NO_MATCH    
    #         = There was no match for the specified key in the index.
    # 1287 :: ERROR_UNIDENTIFIED_ERROR
    #         = Insufficient information exists to identify the cause of failure.
    # 8322 :: ERROR_DS_RANGE_CONSTRAINT
    #         = A value for the attribute was not in the acceptable range of values.

    if eid == 0:
        print('\nERROR_DIVISION_BY_ZERO\nThe system cannot divide by zero.\n')
    elif eid == 2:
        print('\nERROR_FILE_NOT_FOUND\nThe system cannot find the file specified.\n')
    elif eid == 5:
        print('\nERROR_ACCESS_DENIED\nAccess is denied.\n')
    elif eid == 13:
        print('\nERROR_INVALID_DATA\nThe data is invalid.\n')
    elif eid == 161:    
        print('\nERROR_BAD_PATHNAME\nThe specified path is invalid.\n')
    elif eid == 232:
        print('\nERROR_NO_DATA\nThe pipe is being closed.\n')
    elif eid == 677:
        print('\nERROR_EXTRANEOUS_INFORMATION\nToo Much Information.\n')
    elif eid == 1160:
        print('\nERROR_SOURCE_ELEMENT_EMPTY\nThe indicated source element has no media.\n')
    elif eid == 1169:
        print('\nERROR_NO_MATCH\nThere was no match for the specified key in the index.\n')
    elif eid == 1287:
        print('\nERROR_UNIDENTIFIED_ERROR\nInsufficient information exists to identify the cause of failure.\n')
    elif eid == 8322:
        print('\nERROR_DS_RANGE_CONSTRAINT\nA value for the attribute was not in the acceptable range of values.\n')
    else:
        print('\nERROR\nNo idea what happened.\n')
        
    EXIT()

def GOODBYE():

    print("exitcute")

def EXIT():

    print('\nEXIT() BEFORE END')
    GOODBYE()
    raise SystemExit(0) # this always raises SystemExit
    print("Something went horribly wrong")
