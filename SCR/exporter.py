﻿# -*- coding: utf-8 -*-
"""
This is the exporter module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload

import os
import sys
import errno

import numpy as np

# EXTEND PATH
# ..

# INTERNAL
import functions as func  # !! functional routines applicable to many cases
import image as image     # !! loader routines for fetching input data
import plotter as plot    # !! plotter routines

# RELOAD
reload(plot)
reload(func)
reload(image)

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================

# ==============================================================================
# FUNCTIONS TO MANAGE DATA EXPORT
# ==============================================================================
def check_isdir(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
    return True

def dictmap(dict_of_maps,save_to_root,name,**kwargs):
    """
    EXPORT MAPS OF DATA FROM 
    DICT_OF_MAPS :: DICTIONARY CONTAINING A LIST OF NUMPY ARRAYS
    SAVE_TO_ROOT :: BASE UPON WHICH OUTPUT FOLDER IS INQUIRED
    NAME         :: FILENAME STUB
    """
    print('  EXPORT DATASET')
    if not check_isdir(save_to_root+os.path.sep+'Output'):
        print('  FAIL TO HANDLE OUTPUT DIRECTORY')
    # GET KWARGS
    x_dpi = kwargs.get('x_dpi', None)
    y_dpi = kwargs.get('y_dpi', None)
    x_label = kwargs.get('x_label', "X")
    y_label = kwargs.get('y_label', "Y")
    color_label = kwargs.get('color_label', "Z")
    # WRITE OUT
    try:
        for dict_key in dict_of_maps.keys():
            if isinstance(dict_of_maps[dict_key],list):
                for imap,map in enumerate(dict_of_maps[dict_key]):
                    filename_stub = save_to_root+os.path.sep+'Output'+os.path.sep+name+"_"+dict_key+"_"+str(imap)
                    # SAVE AS IMAGES
                    #image.IMWRITE(filename_stub+".tiff", dict_of_maps[dict_key][imap])
                    #image.IMWRITE(filename_stub+".jpg", dict_of_maps[dict_key][imap])
                    # SAVE AS NUMPY
                    np.savetxt(filename_stub+'.ltr', dict_of_maps[dict_key][imap], delimiter=',')
                    # SAVE AS COLORMAP
                    plot.colormap(dict_of_maps[dict_key][imap],filename_stub+".png", x_dpi = x_dpi, y_dpi = y_dpi, x_label = x_label, y_label = y_label, color_label = color_label)                        
            else:
                filename_stub = save_to_root+os.path.sep+'Output'+os.path.sep+name+"_"+dict_key
                # SAVE AS IMAGES
                #image.IMWRITE(filename_stub+".tiff", dict_of_maps[dict_key])
                #image.IMWRITE(filename_stub+".jpg", dict_of_maps[dict_key])
                # SAVE AS NUMPY
                np.savetxt(filename_stub+'.ltr', dict_of_maps[dict_key], delimiter=',')
                # SAVE AS COLORMAP
                plot.colormap(dict_of_maps[dict_key],filename_stub+".png", x_dpi = x_dpi, y_dpi = y_dpi, x_label = x_label, y_label = y_label, color_label = color_label)
        
    except:
        func.ERROR_PRINT(13)