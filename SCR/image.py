﻿# -*- coding: utf-8 -*-
"""
This is the image module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload

import os
import sys
import tkinter as tk

import numpy as np
import cv2
import imutils

from inspect import getsourcefile
from collections import namedtuple as nt

import tifffile as tiff # ! DOC: https://www.lfd.uci.edu/~gohlke/code/tifffile.py.html

# EXTEND PATH
# ..

# INTERNAL
if "coucou_constants" not in globals():
    from constants import *   # !! import all global constants from LIB/constants.py
elif globals()['coucou_constants'] == False:
    from constants import *   # !! import all global constants from LIB/constants.py

import formats as formats # !! import global formats from LIB/formats.py
reload(formats)

import functions as func  # !! functional routines applicable to many cases
reload(func)

import plotter as plot
reload(plot)

# STACKOVERFLOW
from s33293804 import *

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================
# HELPER PARAMETERS
global_list = []
global_bool = False

# METHODOLOGY SELECTORS
# define method used to load images, 
# possible values are
# opencv   :: use the module opencv
# tifffile :: use the module tifffile
global_load_method = 'opencv'

# CONSTANTS
global_avoid_underflow = False
global_fill_screen_percent = 0.9

# ==============================================================================
# FUNCTIONS TO MANAGE RUNTIME ENVIRONMENT
# ==============================================================================

def ISIMG(path):
    try:
        extension = func.GIVE_EXTENSION(path)
        if extension in formats.acceptedinput[global_load_method]:
            return True
        else:
            return False
    except:
        func.ERROR_PRINT(1287)
        
# ==============================================================================
# FUNCTIONS FOR I/O
# ==============================================================================

def IMREAD(path):
    if global_load_method == 'opencv':
        return cv2.imread(path,cv2.IMREAD_UNCHANGED)
    elif global_load_method == 'tifffile':
        return tiff.imread(os.path.abspath(path))
    else:
        func.ERROR_PRINT(1169)
        
def IMSHOW(img, *args, **kwargs):
    name = kwargs.get('name', "ANONYMOUS")
    
    # Routine
    print(' > ENTER IMAGE\n\n**********')
    print('ZOOM IN  BY HOLDING RIGHT MOUSE KEY AND MOVE UP')
    print('ZOOM OUT BY HOLDING RIGHT MOUSE KEY AND MOVE DOWN')
    print('EXIT BY WINDOW-X, q or esc')
    
    window = PanZoomWindow(img, name)
    
    key = -1
    
    while key != ord('q') and key != 27 and cv2.getWindowProperty(window.WINDOW_NAME, 0) >= 0:
        key = cv2.waitKey(5) #User can press 'q' or 'ESC' to exit

    cv2.destroyAllWindows()
    print('**********\n')
 
def IMWRITE(full_name, img):
    try:
        #extension = func.GIVE_EXTENSION(full_name)
        #if extension == 'tiff' or extension == 'tif':
        #    tiff.imsave(full_name, img)
        #else:
        if not cv2.imwrite(full_name, img):
            func.MESSAGE_PRINT('image.py','Issue with "'+full_name.replace(os.path.sep,'/')+'"','Write out to shell directory instead.')
            if not cv2.imwrite(os.path.split(full_name)[1], img):
                func.MESSAGE_PRINT('image.py','Shell directory protected!')
                func.ERROR_PRINT(161)
        return True
    except:
        func.ERROR_PRINT(13)
        
# ==============================================================================
# FUNCTIONS TO MODIFY IMAGES
# ==============================================================================
        
def REDUCEDPI(images,factor,*args, **kwargs):
    keys = kwargs.get('keys', ["wumwum"])
    output = {}
    for key in keys:
        if key != "wumwum":
            oldshape = images[key].shape
            newshape = (np.floor(oldshape[0]/factor).astype(int),np.floor(oldshape[1]/factor).astype(int))
            reduced = np.zeros(newshape,dtype=images[key].dtype)
            for i in range(newshape[0]):
                for j in range(newshape[1]):
                    value = 0.
                    for m in range(factor):
                        for n in range(factor):
                            value = value + images[key][i * factor + m, j * factor + n]
                    reduced[i,j] = value / float(factor**2)
            output[key] = reduced
        else:
            print("  IGNORE KEY 'wumwum'")
    return output
    
def REDUCEDIMENSIONS(img,selector):
    # !TODO np.average supports weights, usefull for option 'A'
    try:
        if selector == 'R':
            return img[:,:,1]
        elif selector == 'G':
            return img[:,:,2]
        elif selector == 'B':
            return img[:,:,3]
        elif selector == 'A':
            return np.average(img,axis=-1)
        else:
            func.ERROR_PRINT(1169)
    except:
        func.ERROR_PRINT(5)
        
def TO_RGB(img):
    # get color
    try:
        img = cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)
    except:
        func.MESSAGE_PRINT('image.py',"Image can not be transformed via GRAY2RGB.")
    return img
    
def TO_8bit(img):
    return ((img-np.nanmin(img))/(np.nanmax(img)-np.nanmin(img))*(2**8-1)).astype("uint8")
        
def ENHANCED_VISIBILITY(img):
    # get color
    img = TO_RGB(img)
    # adjust dynamic range
    #minval = np.nanmin(img)
    #maxval = np.nanmax(img)
    #comfort = 0.001
    #img = np.where(img < maxval*comfort, img, maxval*comfort)
    #img = np.where(img > minval*(1.+comfort), img, minval*(1.+comfort))
    img = TO_8bit(img)
    # return in color scale
    return cv2.applyColorMap(img, cv2.COLORMAP_JET)
    
def FIT_SCREEN(img):
    global global_fill_screen_percent
    # take into account the screen size to display image properly
    root = tk.Tk()
    root.update_idletasks()
    root.attributes('-fullscreen', True)
    root.state('iconic')
    geometry = root.winfo_geometry()
    root.destroy()
    wxh = geometry.split('+')[0]
    sw = float(wxh.split('x')[0])
    sh = float(wxh.split('x')[1])
    # scale to screen size
    scalefactor_h = np.floor(img.shape[0] / sh * global_fill_screen_percent ) + 1
    scalefactor_w = np.floor(img.shape[1] / sw * global_fill_screen_percent ) + 1
    scalefactor = int(max(scalefactor_h,scalefactor_w))
    img = img[::scalefactor,::scalefactor]
    return img, scalefactor
        
# ==============================================================================
# FUNCTIONS TO MANAGE IMAGES
# ==============================================================================

def BITRANGE(dtype):
    if dtype == np.uint8:
        return (0,2**-1)
    elif dtype == np.uint16:
        return (0,2**16-1)
    elif dtype == np.uint32:
        return (0,2**32-1)
    elif dtype == np.float:
        return (-1.,1.)
    elif dtype == np.int8:
        return (-2**7,2**7-1)
    elif dtype == np.int16:
        return (-2**15,2**15-1)
    elif dtype == np.int32:
        return (-2**31,2**31-1)
    else:
        func.MESSAGE_PRINT('image.py',"Bit depth can not be understood.")
        func.ERROR_PRINT(13)
    
def INTERACTIVE_CROP(img, *args, **kwargs):
    # CHECK OPTIONAL PRIORITARIAN INPUT
    name = kwargs.get('name', "ANONYMOUS")
    mode = kwargs.get('mode', "RECTANGULAR") # TODO
    adjust = kwargs.get('adjust', None)
    # CHECK IF PATH OR IMAGE
    if isinstance(img,str):
        img = IMREAD(img)
    # CHECK IF OPTIMIZED DISPLAY AND FIT TO SCREEN
    if adjust != None:
        # adjust dynamic range
        # !TODO
        #img = ((img-np.nanmin(img))/(np.nanmax(img)-np.nanmin(img))*(2**8-1)).astype("uint8")
        # take into account the screen size to display image properly
        img, scalefactor = FIT_SCREEN(img)
    else:
        scalefactor = 1   
    # CROP AREA
    print(' > ENTER IMAGE\n\n**********')
    (x,y,w,h) = cv2.selectROI("ORIGINAL "+name+" - SELECT ROI BY MOUSE KLICK+HOVER AND PRESS ENTER", img)
    cropped = img[y:y+h , x:x+w]
    # VERIFY
    cv2.imshow('CROPPED '+name+" - PRESS ANY KEY TO CONTINUE",cropped)
    # HANDLE USER FEEDBACK
    cv2.waitKey(0)
    print("   PRESS ANY KEY TO CONTINUE")
    cv2.destroyAllWindows()
    print('**********\n')
    
    return {'mode':mode,'corner_x':x*scalefactor,'corner_y':y*scalefactor,'width':w*scalefactor,'height':h*scalefactor}

def INTERACTIVE_LINE(img, *args, **kwargs):
    global global_list, global_bool
    global_list = []
    global_bool = False
    # MOUSE ACTION
    def click_and_get(event, x, y, flags, param):
        '''
        HELPER FUNCTION: CLICK EVENT FUNCTION
        https://www.pyimagesearch.com/2015/03/09/capturing-mouse-click-events-with-python-and-opencv/
        '''
        # grab references to the global variables
        global global_list, global_bool
        # if the left mouse button was clicked, record the starting
        # (x, y) coordinates and set pathing
        if event == cv2.EVENT_LBUTTONDOWN:
            if len(global_list) < 1:
                global_list = [(x, y)]
            elif len(global_list) == 2 : # !! add only to remove in main, click exit strategy
                global_bool = True
            else:
                global_list.append((x, y))
                # draw a line in the open named image outside the function
                cv2.line(img, global_list[-1], global_list[-2], (255, 10, 10), linewidth)
                cv2.imshow(name, img)
        # check to see if the right mouse button was released
        elif event == cv2.EVENT_RBUTTONDOWN:
            # delet last point
            global_list.pop()

    # CHECK OPTIONAL PRIORITARIAN INPUT
    name = kwargs.get('name', "SELECT AXIS :: LEFT MOUSE BUTTON = NEW POINT; SELECT TWO AND CONFIRM BY CLICK")
    adjust = kwargs.get('adjust', None)
    linewidth = kwargs.get('linewidth', 1)
    # OPTIMIZE DISPLAY
    if adjust != None:
        # take into account the screen size to display image properly
        img, scalefactor = FIT_SCREEN(img)
        img = ENHANCED_VISIBILITY(img)
    else:
        scalefactor = 1 
    # COPY FOR RESET
    clone = img.copy()
    img = clone.copy()
    # USER INTERACTION
    print(' > ENTER IMAGE\n\n**********')
    print("  .. PRESS LEFT   MOUSE BUTTON TO ADD NEW POINT")
    print("  .. PRESS RIGHT  MOUSE BUTTON TO DELETE LAST POINT")
    print("  .. PRESS R      KEY          TO RESET")
    print("  .. PRESS H      KEY          TO SELECT CENTRE-HORIZONTAL")
    print("  .. PRESS V      KEY          TO SELECT CENTRE-VERTICAL")
    print("  .. PRESS Q      KEY          TO QUIT")
    print("  .. PRESS C      KEY          TO CONFIRM SELECTION")
    # !TODO add options for purely horizontal or vertical
    # CANVAS
    cv2.namedWindow(name)
    cv2.setMouseCallback(name, click_and_get)
    
    # ACTIVITY
    # keep looping until the 'q' key is pressed
    while True:
        # display the image and wait for a keypress
        cv2.imshow(name, img)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("r"):
            # if the 'r' key is pressed, reset the cropping region
            img = clone.copy()
            global_list = []
        if key == ord("h"):
            # if the 'h' key is pressed, select centre horizontal
            global_list = [(int(0.1*clone.shape[0]),int(clone.shape[0]/2.)),\
                           (int(0.9*clone.shape[0]),int(clone.shape[0]/2.))]
            global_bool = True
        if key == ord("v"):
            # if the 'v' key is pressed, select centre vertical
            global_list = [(int(clone.shape[1]/2.),int(0.1*clone.shape[1])),\
                           (int(clone.shape[1]/2.),int(0.9*clone.shape[1]))]
            global_bool = True
        elif key == ord("c") or key == ord("q"):
            # if the 'c' key is pressed, break from the loop
            break
        if global_bool:
            # if two points are selected everything is fine
            break
    
    # housekeeping
    if len(global_list) < 2:
        print("   YOU FAILED THE TASK: THE LINE SHOULD BE DEFINED BY TWO POINTS!")
        global_list = INTERACTIVE_LINE(img)
    
    cv2.destroyAllWindows()
    print('**********\n')
    
    return np.array(global_list)*scalefactor
    
def INTERACTIVE_POINT(img, *args, **kwargs):
    global global_list
    global_list = []
    
    # CHECK OPTIONAL PRIORITARIAN INPUT
    name = kwargs.get('name', "SELECT POINT :: LEFT MOUSE BUTTON = NEW POINT; PRESS C TO CONFIRM")
    adjust = kwargs.get('adjust', None)
    radius = kwargs.get('radius', 1)
    # OPTIMIZE DISPLAY
    if adjust != None:
        # take into account the screen size to display image properly
        img, scalefactor = FIT_SCREEN(img)
        img = ENHANCED_VISIBILITY(img)
    else:
        scalefactor = 1 
    # COPY FOR RESET
    clone = img.copy()
    img = clone.copy()
    # USER INTERACTION
    print(' > ENTER IMAGE\n\n**********')
    print("  .. PRESS LEFT   MOUSE BUTTON TO ADD NEW POINT")
    print("  .. PRESS R      KEY          TO RESET")
    print("  .. PRESS Q      KEY          TO QUIT")
    print("  .. PRESS C      KEY          TO CONFIRM SELECTION")
    
    # MOUSE ACTION
    def click_and_get(event, x, y, flags, param):
        '''
        HELPER FUNCTION: CLICK EVENT FUNCTION
        https://www.pyimagesearch.com/2015/03/09/capturing-mouse-click-events-with-python-and-opencv/
        '''
        # grab references to the global variables
        global global_list
        # if the left mouse button was clicked, record the starting
        # (x, y) coordinates and set pathing
        if event == cv2.EVENT_LBUTTONDOWN:
            global_list.append((x, y))
            # draw a point in the open named image outside the function
            cv2.circle(img, global_list[-1], radius, (255, 10, 10), -1)
            cv2.imshow(name, img)

    # CANVAS
    cv2.namedWindow(name)
    cv2.setMouseCallback(name, click_and_get)
    
    # ACTIVITY
    # keep looping until the 'q' key is pressed
    while True:
        # display the image and wait for a keypress
        cv2.imshow(name, img)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("r"):
            # if the 'r' key is pressed, reset the cropping region
            img = clone.copy()
            global_list = []
        elif key == ord("c") or key == ord("q"):
            # if the 'c' key is pressed, break from the loop
            break
    
    # housekeeping
    if len(global_list) < 1:
        print("   YOU FAILED THE TASK: NO POINT SELECTED!")
        global_list = INTERACTIVE_POINT(img)
    
    cv2.destroyAllWindows()
    print('**********\n')

    #Multiply each element to avoid duclipate the tupla
    global_list[-1]=(scalefactor*global_list[-1][0],scalefactor*global_list[-1][1])
    

    return np.array(global_list[-1])

    
# ==============================================================================
# FUNCTIONS TO CREATE IMAGES
# ==============================================================================
def GIVE_MASK(mode, shape, *args, **kwargs):
    """
    GIVES A BINARY MASK ARRAY
    MODE  :: TYPE OF MASK
    SHAPE :: 2D SHAPE OF OUTPUT ARRAY
    """
    # CHECK OPTIONAL PRIORITARIAN INPUT
    lower_value = kwargs.get('lower', 0)
    higher_value = kwargs.get('higher', 1)
    dia = kwargs.get('dia', '+') # !! accepts '+' for positive and '-' for negative
    dtype = kwargs.get('dtype', np.uint16)
    # GIVE MASK
    # invert
    if dia == "-":
        var = lower_value
        lower_value = higher_value
        higher_value = var
    # canvas
    mask = np.ones(shape, dtype=dtype) * lower_value
    # draw
    if mode == 'RECTANGULAR':
        corner_x = kwargs.get('corner_x', 0)
        corner_y = kwargs.get('corner_y', 0)
        w = kwargs.get('width', shape[1]-1)
        h = kwargs.get('height', shape[0]-1)
        mask[corner_y:corner_y+h,corner_x:corner_x+w] = higher_value
        #plot.imshow(mask[:,:,0])
        #plot.imshow(mask[:,:,1])
    elif mode == 'ELLIPTICAL':
        corner_x = kwargs.get('corner_x', 0)
        corner_y = kwargs.get('corner_y', 0)
        w = kwargs.get('width', shape[1]-1)
        h = kwargs.get('height', shape[0]-1)
        for i in range(h):
            for j in range(w):
                if (i-h/2.)**2/(h/2.)**2 + (j-w/2.)**2/(w/2.)**2 <= 1.:
                    mask[corner_y+i,corner_x+j] = higher_value
    else:
        func.ERROR_PRINT(1169)
    return mask
    
# ==============================================================================
# AUTOMATIC INTELLIGENCE
# ==============================================================================
# ..
            
# ==============================================================================
# FUNCTIONS FOR MATH
# ==============================================================================
# ..