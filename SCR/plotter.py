﻿# -*- coding: utf-8 -*-
"""
This is the plotter module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
from importlib import reload

import os
import sys

import numpy as np

import matplotlib as mpl
mpl.use('TKAgg')
import matplotlib.pyplot as plt
#from mpl_toolkits import axes_grid1
from matplotlib.colors import LogNorm,SymLogNorm

from mpl_toolkits.axes_grid1 import make_axes_locatable

# Style
mpl.style.use('classic')
mpl.rcParams['image.cmap'] = 'viridis'
print_colormap = 'viridis'
graymap = 'gray'
#mpl.rcParams['font.family'] = 'FrontPage Pro'

mpl.rcParams['grid.color'] = 'k'
mpl.rcParams['grid.linestyle'] = ':'
mpl.rcParams['grid.linewidth'] = 0.5

mpl.rcParams['figure.figsize'] = [8.0, 6.0]
mpl.rcParams['figure.dpi'] = 80
mpl.rcParams['savefig.dpi'] = 600

mpl.rcParams['font.size'] = 20
mpl.rcParams['legend.fontsize'] = 'xx-small'
mpl.rcParams['figure.titlesize'] = 'medium'

# EXTEND PATH
# ..

# INTERNAL
import functions as func  # !! functional routines applicable to many cases

# RELOAD
reload(func)

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================
# default values for plot styles
plot_spectrum_xaxis_default = 'log'
plot_spectrum_xaxis_max_default = None

# ==============================================================================
# FUNCTIONS TO MANAGE DATA EXPORT
# ==============================================================================
def colormap(twoDmap,save_to,*args, **kwargs):
    # Options
    x_dpi = kwargs.get('x_dpi', None)
    y_dpi = kwargs.get('y_dpi', None)
    x_label = kwargs.get('x_label', "X")
    y_label = kwargs.get('y_label', "Y")
    color_label = kwargs.get('color_label', "Z")
    color_scale = kwargs.get('color_scale', "lin")
    color_range = kwargs.get('color_range', None)
    
    silent = kwargs.get('silent', False)
    
    # Plot
    fig, axs = plt.subplots(nrows=1, ncols=1)
    fig.set_tight_layout(True)
    
    if x_dpi == None:
        axs.set_xlabel(x_label)
        x_dpi = 1
    else:
        axs.set_xlabel(x_label+" / [ dot/"+str(x_dpi)+"dpi]")
    if y_dpi == None:
        axs.set_ylabel(y_label)
        y_dpi = 1
    else:
        axs.set_ylabel(y_label+" / [ dot/"+str(y_dpi)+"dpi]")
    
    x_lins = np.arange(0,twoDmap.shape[1],1)/x_dpi
    y_lins = np.arange(0,twoDmap.shape[0],1)/y_dpi
    
    (ax_x,ax_y) = np.meshgrid(x_lins, y_lins)
    
    if isinstance(color_range,list):
        range = color_range
    else:
        range = [np.nanmin(twoDmap),np.nanmax(twoDmap)]
    
    levels = 20
    c_tik = np.linspace(range[0],range[1],levels)
    
    if color_scale == 'symlog':
        #c_tik = np.logspace(np.sign()*np.log2(np.abs(np.nanmin(twoDmap))),np.sign(np.nanmax(twoDmap))*np.log2(np.abs(np.nanmax(twoDmap))), num=levels, base=2.0)
        norm  = SymLogNorm(linthresh=0.00003, linscale=0.01,\
                           vmin=np.nanmin(twoDmap), vmax=np.nanmax(twoDmap), base=10)
        
    tmp = axs.contourf(ax_x, ax_y, twoDmap, vmin=range[0],vmax=range[1], levels=levels)
    
    if color_scale == 'log' or color_scale == 'symlog':
        getty = axs.contour(ax_x, ax_y, twoDmap, vmin=range[0],vmax=range[1], levels = tmp.levels[::2], colors='w', linewidths=2, norm = norm)
        cbar = fig.colorbar(tmp, ax=axs, shrink=0.95, ticks = c_tik)
    else:
        getty = axs.contour(ax_x, ax_y, twoDmap, vmin=range[0],vmax=range[1], levels = tmp.levels[::2], colors='w', linewidths=2)
        cbar = fig.colorbar(tmp, ax=axs, shrink=0.95, ticks = c_tik, format="%.2e")

    cbar.ax.set_ylabel(color_label)
    cbar.add_lines(getty)
    
    if save_to != None:
        if os.path.isfile(save_to):
            os.remove(save_to)
        plt.savefig(save_to)
    if not silent: plt.show()
    plt.close()
    
    return True
    
def plot2D(x_array,y_arrays,save_to,*args, **kwargs):

    # Options
    x_label = kwargs.get('x_label', "X")
    y_label = kwargs.get('y_label', "Y")
    
    title = kwargs.get('title', None)
    
    x_scale = kwargs.get('x_scale', "linear")
    y_scale = kwargs.get('y_scale', "linear")
    
    x_max = kwargs.get('x_max', None)
    
    labels = kwargs.get('labels', None)
    
    silent = kwargs.get('silent', False)
    
    # Plot
    fig, axs = plt.subplots(nrows=1, ncols=1)
    fig.set_tight_layout(True)
    
    axs.set_xlabel(x_label)
    axs.set_ylabel(y_label)
    
    if isinstance(title,str):
        axs.set_title(title)
    
    axs.set_xscale(x_scale)
    axs.set_yscale(y_scale)  
    
    if isinstance(y_arrays[0],list) or isinstance(y_arrays[0],np.ndarray):
        if not isinstance(labels,list) and not isinstance(labels,np.ndarray):
            labels = []
            for y_array in y_arrays:
                labels.append(None)
        for y_array,label in zip(y_arrays,labels):
            if x_max != None:
                here = np.where(x_array < x_max)
                axs.plot(x_array[here],y_array[here],label=label)
            else:
                axs.plot(x_array,y_array,label=label)
    else:
        if x_max != None:
            here = np.where(x_array < x_max)
            axs.plot(x_array[here],y_arrays[here],label=labels)
        else:
            axs.plot(x_array,y_arrays,label=labels)
    
    axs.legend()
    
    # Save
    if save_to != None:
        if os.path.isfile(save_to):
            os.remove(save_to)
        plt.savefig(save_to)
    if not silent: plt.show()
    plt.close()

    return True
    
def mapping(img,*args,**kwargs):
    # CHECK OPTIONAL PRIORITARIAN INPUT
    name = kwargs.get('name', "Input Image")
    clabel = kwargs.get('clabel', "arb.u.")
    # PLOT
    ax = plt.subplot()
    ax.set_title(name)
    im = ax.imshow(img, cmap = print_colormap)

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cax.set_xlabel(clabel)

    plt.colorbar(im, cax=cax)

    plt.show()
    plt.close()
    
def imshow(img,*args,**kwargs):       
    # CHECK OPTIONAL PRIORITARIAN INPUT
    name = kwargs.get('name', "Input Image")
    # PLOT
    plt.imshow(img, cmap = graymap)
    plt.title(name), plt.xticks([]), plt.yticks([])
    plt.show()
    plt.close()
    
def imcompare(img1,img2,*args,**kwargs):
    # CHECK OPTIONAL PRIORITARIAN INPUT
    name = kwargs.get('name', "Compare")
    name1 = kwargs.get('name1', "Image 1")
    name2 = kwargs.get('name2', "Image 2")
    renorm = kwargs.get('norm', None)
    # PLOT
    fig, axes = plt.subplots(nrows=1, ncols=2)
    plt.title(name)
    im = axes[0].imshow(img1, cmap = graymap)
    axes[0].set_title(name1), axes[0].set_xticks([]), axes[0].set_yticks([])
    im = axes[1].imshow(img2, cmap = 'gray')
    axes[1].set_title(name2), axes[1].set_xticks([]), axes[1].set_yticks([])
    plt.show()
    plt.close()